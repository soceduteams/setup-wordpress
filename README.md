# Các Plugin cần cài khi sử dụng Wordpress #

Các Plugin căn bản cần sử dụng khi cài Wordpress

### Askimet ###

* Chống Spam


### iThemes Security ###

* Bảo mật dành cho Wordpress


### SMTP ###
* Custome gửi email qua SMTP
* SES hoặc Sendgrid


### Cài themes ###
* Cài theme mình muốn


### Advanced TinyMCE ###

* Bổ sung các nút soạn thảo vào WordPress.


### Shortcodes Ultimate ###

* Bổ sung Shortcodes.


### Yoast SEO ###

* Hỗ trợ SEO + gợi ý SEO


### WP Supper Cache (Litespeed Cache nếu Hosting có hỗ trợ) ###

* Cache Wordpress


### Google Tag Manager  ###
* Google Tag
* by Thomas Geiger


### Contact Form 7 (nếu cần Form)  ###
* Tạo From liên hệ


### AdRotate ###
* plugin tạo quảng cáo

### KingSumo Giveaways ###
* Plugin Giveaways Share trên Social


### Compress JPEG & PNG images ###
* Nén hình nhỏ, để giúp Web load nhanh hơn
* by TinyPNG


### WordPress Importer ###
* Để insert Sample Data (cần mới cài)

### WordPress Reset ###
* Reset lại Wordpress như ban đầu
* Các Plugin sẽ bị deactive
* Link: https://wordpress.org/plugins/wordpress-reset/



